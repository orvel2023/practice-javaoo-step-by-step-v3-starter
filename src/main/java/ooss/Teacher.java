package ooss;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class Teacher extends Person {
    private List<Klass> teachingClass = new ArrayList<>();
    public Teacher(int id, String name, int age) {
        super(id, name, age);
    }
    public String introduce() {
        if (teachingClass.isEmpty()) {
            return String.format( "My name is %s. I am %d years old. I am a teacher.",name, age);
        }
        String teachClassString = teachingClass.stream()
                .map(klass -> klass.getClassNumber())
                .collect(Collectors.toList())
                .stream()
                .map(Objects::toString)
                .collect(Collectors.joining(", "));
        return String.format( "My name is %s. I am %d years old. I am a teacher. I teach Class ",name, age)
                .concat(teachClassString)
                .concat(".");
    }
    public void introduceOfClassLeaterChange(Klass klass) {
        System.out.println(String.format("I am %s, teacher of Class %d. I know %s become Leader.",name,klass.getClassNumber(),klass.getClassLeater().name));
    }
    public void assignTo(Klass klass){
        if(!this.teachingClass.contains(klass)){
            this.teachingClass.add(klass);
        }
    }
    public boolean belongsTo(Klass kclass){
        if(!teachingClass.isEmpty()&&teachingClass.contains(kclass)){
            return true;
        }
        return false;
    }
    public boolean isTeaching(Student student) {
        return !this.teachingClass.stream().filter(klass -> student.isIn(klass)).collect(Collectors.toList()).isEmpty();

    }
}
