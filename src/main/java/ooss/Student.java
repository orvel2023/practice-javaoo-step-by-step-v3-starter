package ooss;

public class Student extends Person {
    private Klass belongClass;
    private boolean isLeater = false;
    public Student(int id, String name, int age) {
        super(id, name, age);
    }
    public void setLeater(boolean leater) {
        isLeater = leater;
    }
    public String introduce() {
        if(belongClass == null){
            return  String.format( "My name is %s. I am %d years old. I am a student.",name, age);
        }
        if(isLeater){
            return String.format( "My name is %s. I am %d years old. I am a student. I am the leader of class %d.",name, age,belongClass.getClassNumber());
        }
            return String.format( "My name is %s. I am %d years old. I am a student. I am in class %d.",name, age,belongClass.getClassNumber());
    }
    public void introduceOfClassLeaterChange() {
        System.out.println(String.format("I am %s, student of Class %d. I know %s become Leader.",name,belongClass.getClassNumber(),belongClass.getClassLeater().name));
    }
    public void join(Klass kclass){
        this.belongClass = kclass;
    }
    public boolean isIn(Klass kclass){
        if(belongClass!=null&&belongClass.equals(kclass)){
            return true;
        }
        return false;
    }
}
