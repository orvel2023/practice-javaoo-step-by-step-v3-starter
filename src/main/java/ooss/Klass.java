package ooss;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Klass {

    private int classNumber;
    private Student classLeater;
    private List<Student> classStudent = new ArrayList<>();
    private List<Teacher> classTeacher = new ArrayList<>();
    public Klass(int classNumber) {
        this.classNumber = classNumber;
    }
    public Student getClassLeater() {
        return classLeater;
    }
    public int getClassNumber() {
        return classNumber;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(classNumber);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (obj instanceof Klass) {
            Klass otherClass = (Klass) obj;
            if (this.classNumber == otherClass.classNumber) {
                return true;
            }
        }
        return false;
    }
    public void assignLeader(Student student) {
        if (student.isIn(this)) {
            this.classLeater = student;
            student.setLeater(true);
            classStudent.stream().forEach(student1 -> student1.introduceOfClassLeaterChange());
            classTeacher.stream().forEach(teacher -> teacher.introduceOfClassLeaterChange(this));
        }
        System.out.println("It is not one of us.");
    }

    public boolean isLeader(Student tom) {
        return this.classLeater.equals(tom);
    }

    public void attach(Teacher teacher) {
        if(!this.classTeacher.contains(teacher)) this.classTeacher.add(teacher);
    }
    public void attach(Student student) {
        if(!this.classStudent.contains(student)) this.classStudent.add(student);

    }

}
