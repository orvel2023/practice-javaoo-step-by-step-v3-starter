package ooss;

import java.util.Objects;

public class Person {
    private int id;
    protected String name;
    protected int age;

    public Person(int id, String name, int age) {
        this.age = age;
        this.id = id;
        this.name = name;
    }
    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }
    @Override
    public boolean equals(Object obj) {
        if(this == obj){
            return true;
        }
        if(obj == null){
            return false;
        }
        if(obj instanceof Person){
            Person otherPerson = (Person) obj;
            if(this.id == otherPerson.id){
                return true;
            }
        }
        return false;
    }
    public String introduce() {

        return String.format( "My name is %s. I am %d years old.",name, age);
    }
}
