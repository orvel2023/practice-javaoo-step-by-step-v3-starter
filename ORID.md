# O
  - Code View : Why do I need to do a code review? Because code review can absorb good suggestions and opinions from others, find and correct errors in time, reduce the chance of bugs and improve the quality of code.
  -  Code Review should discuss functionality implementation, architectural design, and code quality.
  - Java 8 Stream : A pipeline operation, which can carry out various very convenient and efficient aggregation operations on collection objects, and can greatly improve programming efficiency and program readability with the help of the newly emerged Lambda expression.
  - OOP : The three main features of OOP are encapsulation, inheritance, and polymorphism. These three features allow for improved code reusability and high cohesion. Using OOP makes the code structure clear and easier to extend and maintain.

# R
  - Challenging !
# I
  - I think today's lesson has given me a deeper understanding of stream and oop in java.The biggest significance is that I will use stream and oop in future development to improve development efficiency and quality.
# D
  - Use oop and stream for daily development.
  - Pay attention to code naming and code specification during the development process.
  - Learn the Java language in depth.

